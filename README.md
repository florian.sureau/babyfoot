# Firebase test

## Install

After cloning the repo:

Install dependencies: 
`npm install && npm install --only=dev`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Links
* Google firebase: (https://firebase.google.com)[https://firebase.google.com]
* Angular firebase: (https://github.com/angular/angularfire2)[https://github.com/angular/angularfire2]
* Angular Material: (https://material.angular.io/)[https://material.angular.io/]

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { AuthService } from '../services/auth.service';
import { GamesService, IGame } from '../services/games.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  displayedColumns = ['player1', 'player2', 'score', 'date'];
  dataSource: MatTableDataSource<IGame>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private authService: AuthService,
    private gamesService: GamesService
  ) { }

  ngOnInit() {
    this.gamesService.getGames().subscribe(elements => {
      this.dataSource = new MatTableDataSource(elements);
      this.dataSource.sort = this.sort;
    });
  }
}

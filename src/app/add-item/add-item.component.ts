import { Component } from '@angular/core';
import { GamesService } from '../services/games.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent {

  date = new FormControl(new Date());

  player1 = '';
  player2 = '';
  score1 = 10;
  score2 = 0;

  constructor(private gamesService: GamesService) { }

  onSave() {
    this.gamesService.add(this.player1, this.player2, this.score1, this.score2, Date.parse(this.date.value));
  }
}

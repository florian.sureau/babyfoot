import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {

  constructor(public auth: AuthService) { }

  OnLoginClick(): void {
    this.auth.googleLogin();
  }

  OnLogoutClick(): void {
    this.auth.logout();
  }
}

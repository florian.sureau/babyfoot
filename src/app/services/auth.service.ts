import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthService {
  public user: Observable<firebase.User>;

  constructor(public af: AngularFireAuth, private snackbar: MatSnackBar) { }

  registerAuthChange() {
    this.user = this.af.authState;

    this.af.auth.onAuthStateChanged(auth => {
      if (auth) {
        this.snackbar.open('User authentified.', 'Info', { 'duration': 1000 });
      } else {
        this.snackbar.open('User signed out.', 'Info', { 'duration': 1000 });
      }
    });
  }

  googleLogin() {
    this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.af.auth.signOut();
  }
}

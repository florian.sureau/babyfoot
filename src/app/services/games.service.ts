import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

export interface IGameWithId extends IGame { id: string; }

export interface IGame {
  player1: string;
  player2: string;
  score1: number;
  score2: number;
  userId: string;
  date: number;
}

@Injectable()
export class GamesService {
  games: Observable<IGame[]>;

  constructor(
    private db: AngularFirestore,
    private authService: AuthService,
    private snackbar: MatSnackBar) {
    this.authService.user.subscribe(auth => {
      if (auth != null) {
        this.games = this.db.collection<IGame>('games').valueChanges();
      }
    });
  }

  add(player1: string, player2: string, score1: number, score2: number, date: number = Date.now()) {
    this.db.collection<IGame>('games').add({
      'player1': player1,
      'player2': player2,
      'score1': score1,
      'score2': score2,
      'date': date,
      'userId': this.authService.af.auth.currentUser.uid
    }).then(value => {
      this.snackbar.open('Game added.', '', { 'duration': 1000 });
    }).catch(error => {
      this.snackbar.open(error, 'Error', { 'duration': 1000 });
    })
      ;
  }

  getGames() {
    this.games = this.db.collection<IGame>('games').valueChanges();
    return this.games;
  }
}
